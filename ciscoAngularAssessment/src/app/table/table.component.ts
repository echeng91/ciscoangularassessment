import { Component, OnInit } from '@angular/core';
import * as sampleData from '../sample_data.json';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})

export class TableComponent implements OnInit {
  dataItems = (sampleData as any).default; // handles case wherein json is shoved into default
  displayedItems: any[];
  perPage: number;
  numPages: number;
  pageArray: number[];
  currentPage: number;
  firstItem: number;
  lastItem: number;
  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.perPage = 50;
    this.onPerPageChange(this.perPage);
  }

  // calculates number of pages and calls other functions
  onPerPageChange(perPage: number) {
    // only perform calculation if perPage is a number
    if (!isNaN(perPage) && perPage > 0) {
      console.log(perPage);
      this.perPage = perPage;
      this.numPages = Math.floor( this.dataItems.length / this.perPage);
      if (this.dataItems.length % this.perPage !== 0) {
        this.numPages++;
      }
      this.makePageArray();
      this.onPageChange(1); // send to first page on change of number of rows to show per page
    }
  }

  // determines first item and last item to be displayed on page
  onPageChange(page: number) {
    this.currentPage = page;
    this.firstItem = (page - 1) * this.perPage;
    this.lastItem = page * this.perPage;
    this.displayedItems = this.dataItems.slice(this.firstItem, this.lastItem);
  }

  // populates array with values for footer
  makePageArray() {
    this.pageArray = new Array(this.numPages);
    let i: number;
    for (i = 0; i < this.numPages; i++) {
      this.pageArray[i] = i + 1;
    }
  }

  // submit row id and status to /api/submit as a POST request
  submitRow(rowId: number, rowStatus: string) {
    return this.http.post<any>('/api/submit', {if: rowId, status: rowStatus}).pipe(
      // handle submission here
    );
  }
}
